/*
 This file is part of the jquery-tc library.

 jquery-tc library is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 jquery-tc library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with jquery-tc library.  If not, see <http://www.gnu.org/licenses/>.
 */

var slideDuration = 500;
var easement = "easeInOutElastic";
var pendingHover = null;
var activePanel = null;
var openTab = null;

$(document).ready(function() {

    mouse.init();

    $("#sidePanel").velocity({
        properties: {
            left: -300
        },
        options: {
            duration: 100,
            complete: function() {
                SlideTabIn($("#oilsTab"), 0);
                SlideTabIn($("#ailmentsTab"), 60);
                SlideTabIn($("#emotionsTab"), 120);

                InitializeSidePanelTabs();
            }
        }
    })


});

function SlideTabIn(tab, delay) {
    $(tab).velocity({
        properties: {
            left: "5px;"
        },
        options: {
            delay: delay,
            duration: 180,
            easing: easement
        }
    });
}

function InitializeSidePanelTabs() {
    $("#tabCanister").on("click", ".tab", function(){
        SlidePanel($(this));
    }).on("mouseenter", ".tab", function() {
        TabHover($(this));
    }).on("mouseleave", ".tab", function() {
        TabEndHover($(this));
    });
}


function SlidePanel(tab) {
    var tabLeft = 305;
    var slidePanelLeft = 0;
    var panelClass = $(tab).attr("id");

    if ($(tab).offset().left > 100) {
        tabLeft = 5;
        slidePanelLeft = -300;
        panelClass = "";
        TabLockClear($(tab));
        openTab = null;
    } else {
        TabLockHover($(this));
        openTab = tab;
        DisplayTabContent(tab);
        AddPanelClass(panelClass);
    }


    $(tab).velocity("stop");
    $(tab).velocity({
        properties: {
            left: tabLeft
        },
        options: {
            duration: slideDuration,
            easing: easement
        }
    });
    $(sidePanel).velocity("stop");
    $(sidePanel).velocity({
        properties: { left: slidePanelLeft },
        options: {
            duration: slideDuration,
            easing: easement,
            complete: function() {
                if (panelClass == "") {
                    AddPanelClass(panelClass);
                }
            }
        }
    });
}

function DisplayTabContent(tab) {
    $("#sidePanel").find(".panelCanister").hide();
    switch($(tab).attr("id")) {
        case "oilsTab":
            $("#oilsPanelCanister").show();
            break;
        case "emotionsTab":
            $("#emotionsPanelCanister").show();
            break;
        case "ailmentsTab":
            $("#ailmentsPanelCanister").show();
            break;
    }
}

function TabHover(tab) {
    if ($(tab).attr("data-inMotion") != "true" && activePanel == null) {
        $(tab).attr("data-inMotion", "true");
        $(tab).find(".tabHover")
            .css({
                "fontSize" : "2px",
                "z-index": 92,
                "display": "block"
            })
            .velocity({
                properties: {
                    "fontSize": "32px",
                    "z-index": 95
                },
                options: {
                    duration: 300,
                    easing: easement,
                    complete: function() {
                        $(tab).find(".tabHover").velocity({
                            properties: {
                                "fontSize": "24px"
                            },
                            options: {
                                duration: 100,
                                delay: 15,
                                easing: easement,
                                complete: function() {
                                    $(tab).attr("data-inMotion", "false");
                                    if (!mouse.isOver($(tab), true)) {
                                        TabEndHover(tab);
                                    }
                                }
                            }
                        })
                    }
                }
            });
    } else {
        if (activePanel == null) {
            pendingHover = $(tab);
        }

    }
}

function TabEndHover(tab) {
    if ($(tab).attr("data-inMotion") != "true" && activePanel == null) {
        $(tab).attr("data-inMotion", "true");
        $(tab).find(".tabHover")
            .css("z-index", 92)
            .velocity({
                properties: {
                    "fontSize": "2px"
                },
                options: {
                    duration: 200,
                    easing: easement,
                    complete: function() {
                        $(tab).find(".tabHover").css("display", "none");
                        $(tab).attr("data-inMotion", "false");
                        if(pendingHover != null && $(pendingHover).attr("id") == $(tab).attr("id") && mouse.isOver($(tab), true)) {
                            TabHover($(tab));
                        }
                        pendingHover = null;
                    }
                }
            });
    }
}

function TabLockHover(tab) {
    activePanel = $(tab);
    $(tab).find(".tabHover")
        .css({
            "z-index": 95,
            "fontSize": "24px"
        });
}

function TabLockClear(tab) {
    activePanel = null;
    TabEndHover($(tab));
}

function AddPanelClass(className) {
    $("#sidePanel").removeClass("oilsTab ailmentsTab emotionsTab").addClass(className);
}