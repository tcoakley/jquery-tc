/*
 This file is part of the jquery-tc library.

 jquery-tc library is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 jquery-tc library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with jquery-tc library.  If not, see <http://www.gnu.org/licenses/>.
 */
var mouse = {
    mouseX: null,
    mouseY: null,
    inWindow: true,
    init: function() {
        $(document).on('mousemove', function(event) {
            mouse.mouseX = event.pageX;
            mouse.mouseY = event.pageY;
            inWindow = true;
        });
        $('body').on('mouseleave', function() {
            inWindow = false;
        });
    },
    isOver: function($element, rotated) {
        $elementPosition = $($element).offset();
        if (rotated == undefined || !rotated) {
            $elementWidth = $($element).outerWidth();
            $elementHeight = $($element).outerHeight();
        } else {
            $elementWidth = $($element).outerHeight();
            $elementHeight = $($element).outerWidth();
        }

        if (mouse.mouseX !== null) {
            if (mouse.mouseX < $elementPosition.left) { return false; }
            if (mouse.mouseY < $elementPosition.top) { return false; }
            if (mouse.mouseX > $elementPosition.left + $elementWidth) { return false; }
            if (mouse.mouseY > $elementPosition.top + $elementHeight) { return false; }
            if (!inWindow) { return false; }
        }

        return true;
    }
}
