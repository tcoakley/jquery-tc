<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>jquery-tc</title>

    <link rel="stylesheet" type="text/css" href="_css/reset.css" />
    <link rel="stylesheet" type="text/css" href="_less/index.css" />
    <link rel="stylesheet" type="text/css" href="_css/hoverBounce.css" />
    <link href='http://fonts.googleapis.com/css?family=Doppio+One|Michroma' rel='stylesheet' type='text/css'>

    <script src="_js/jquery-1.11.1.min.js" type="text/javascript"></script>
    <script src="_js/velocity-1.1.0.min.js" type="text/javascript"></script>

    <script src="_js/jquery-tc-mouse.1.0.0.js" type="text/javascript"></script>
    <script src="_js/jquery-tc-hoverBounce.1.0.0.js" type="text/javascript"></script>

</head>
<body>

<div class="contentWrapper">
    <div class="content">
        <div class="header">jquery-tc</div>
        <div>
            <img src="_img/gplv3-127x51.png" class="gnuImage">
            I have meant to start putting out some of my code for people to use for a long time.  I just created
            this lovely bouncing text hover effect, decided not to use it in my current location and figured today
            is the day to start sharing.  Everything here is covered under the GNU-GPL.
        </div>

    </div>
</div>

</body>
</html>